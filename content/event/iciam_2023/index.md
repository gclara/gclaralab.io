---
title: 10th International Congress on Industrial and Applied Mathematics

location: Waseda University, Tokyo, Japan

date: '2023-08-20'
date_end: '2023-08-25'

links:
  - name: Details
    url: https://iciam2023.org/registered_data?id=00638
  - name: Slides
    url: uploads/23_08_24_iciam.pdf
---