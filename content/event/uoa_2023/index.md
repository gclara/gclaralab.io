---
title: Seminar of Statistics & Operations Research (UoA)

location: National and Kapodistrian University, Athens, Greece

date: '2023-10-13'
date_end: ''

links:
  - name: Details
    url: http://users.uoa.gr/~strevezas/Seminar-Stat-OR/
  - name: Slides
    url: uploads/23_10_13_uoa.pdf
---