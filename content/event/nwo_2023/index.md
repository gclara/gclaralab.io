---
title: AI & Mathematics PhD Networking Event

location: Dutch Research Council (NWO), Utrecht, The Netherlands

date: '2023-11-09'
date_end: ''

links:
  - name: Details
    url: https://aimath.nl/
  - name: Slides
    url: uploads/23_11_09_aim.pdf
---