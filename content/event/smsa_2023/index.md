---
title: 15th Workshop on Stochastic Models, Statistics and Their Applications (SMSA 2024) 

location: Delft University of Technology, Delft, The Netherlands

date: '2024-03-13'
date_end: '2024-03-15'

links:
  - name: Details
    url: https://sites.google.com/view/smsa2024
  - name: Slides
    url: uploads/24_03_15_smsa.pdf
---