---
title: Mathematics and Data Science Seminar (UTwente)

location: Universiteit Twente, Enschede, The Netherlands

date: '2023-09-04'
date_end: ''

links:
  - name: Details
    url: https://sites.google.com/view/seminarsmds/home-page/
  - name: Slides
    url: uploads/23_09_04_mds.pdf
---