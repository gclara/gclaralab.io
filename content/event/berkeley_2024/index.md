---
title: Simons Institute for the Theory of Computing (Research Visit) 

location: University of California, Berkeley, USA

date: '2024-10-14'
date_end: '2024-11-15'

links:
  - name: Details
    url: https://simons.berkeley.edu/homepage
---