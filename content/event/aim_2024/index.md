---
title: AI & Mathematics Workshop Utrecht

location: Utrecht University, Utrecht, The Netherlands

date: '2024-06-13'
date_end: '2024-03-14'

links:
  - name: Details
    url: https://aimath.nl/index.php/2024/03/29/aim-2024-uu/
  - name: Slides
    url: uploads/24_06_13_aim.pdf
---