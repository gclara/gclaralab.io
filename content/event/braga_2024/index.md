---
title: 6th International Symposium on Nonparametric Statistics (ISNPS 2024)

location: Braga, Portugal

date: '2024-06-25'
date_end: '2024-06-29'

links:
  - name: Details
    url: https://www.easyacademia.org/isnps2024
  - name: Slides
    url: uploads/24_06_28_isnps.pdf
---