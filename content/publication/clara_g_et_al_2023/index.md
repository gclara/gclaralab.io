---
title: "Dropout Versus $\\ell_2$-Penalization in the Linear Model"

authors:
  - gclara
  - Sophie Langer
  - Johannes Schmidt-Hieber
 
date: 2024-07-31

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

publication: Journal of Machine Learning Research

links:
  - name: JMLR
    url: https://www.jmlr.org/papers/v25/23-0803.html
  - name: ArXiv
    url: https://arxiv.org/abs/2306.10529
---