---
title: "sparsevb: Spike-and-Slab Variational Bayes for Linear and Logistic Regression"

authors:
  - gclara
  - Botond Szabó
  - Kolyan Ray
 
date: 2021-01-15

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

publication: The Comprehensive R Archive Network

links:
  - name: CRAN
    url: https://cran.r-project.org/web/packages/sparsevb/index.html
  - name: GitLab
    url: https://gitlab.com/gclara/varpack
---