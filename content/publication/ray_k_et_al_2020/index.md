---
title: Spike and slab variational Bayes for high dimensional logistic regression

authors:
  - Kolyan Ray
  - Botond Szabó
  - gclara
 
date: 2020-12-06

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

publication: Advances in Neural Information Processing Systems 33

links:
  - name: NeurIPS
    url: https://papers.nips.cc/paper_files/paper/2020/hash/a5bad363fc47f424ddf5091c8471480a-Abstract.html
  - name: ArXiv
    url: https://arxiv.org/abs/2010.11665
---