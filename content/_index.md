---
date: "2022-10-24"
sections:
- block: about.biography
  content:
    username: gclara
  id: about
- block: collection
  id: talks
  content:
    title: Upcoming Talks and Attendances
    count: 5
    filters:
      folders:
        - event
    sort_by: 'Date'
    sort_ascending: false
  design:
    columns: '2'
    view: compact 
- block: collection
  id: publications
  content:
    title: Recent Publications
    filters:
      folders:
        - publication
    sort_by: 'Date'
    sort_ascending: false
  design:
    columns: '2'
    view: citation 
- block: experience
  id: experience
  content:
    date_format: Jan 2006
    items:
    - company: Universiteit Twente
      company_url: "https://www.utwente.nl/"
      date_end: ""
      date_start: "2021-09-01"
      description: |2-
          * Thesis (provisional title): *Statistical Theory of Randomized Gradient Descent*
          * Supervisors: [Johannes Schmidt-Hieber](https://wwwhome.ewi.utwente.nl/~schmidtaj/) and [Sophie Langer](https://sites.google.com/view/sophielanger/start)
      location: The Netherlands
      title: PhD in Mathematics
    - company: Vrije Universiteit Amsterdam
      company_url: "https://vu.nl/"
      date_end: "2021-08-31"
      date_start: "2018-09-01"
      description: |2-
          * Thesis: *Sparse Variational Inference and Bayesian High-Dimensional Regression*
          * Supervisor: [Botond Szabó](https://botondszabo.com/)
          * Honours: Cum Laude (Highest Distinction)
      location: The Netherlands
      title: MSc in Mathematics
    - company: University of Massachusetts Amherst
      company_url: "https://www.umass.edu/"
      date_end: "2018-05-31"
      date_start: "2014-09-01"
      location: United States of America
      title: BSc in Mathematics
    title: Education
  design:
    columns: "2"
    view: compact 
- block: contact
  content:
    autolink: false
    email: g "dot" clara "at" utwente "dot" nl
    directions: Zilverling, Hallenweg 19, 7522 NH Enschede, The Netherlands
    coordinates:
      latitude: 52.23907
      longitude: 6.85714
    title: Contact
  design:
    columns: "2"
    view: compact
  id: contact
title: null
type: landing
---
