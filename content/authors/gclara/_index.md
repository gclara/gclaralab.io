---
first_name: Gabriel
highlight_name: true
last_name: Clara
organizations:
  - name: Statistics Group, Universiteit Twente
    url: https://www.utwente.nl/en/eemcs/stat/
role: PhD Student
social:
  - display:
      header: true
  - icon: google-scholar
    icon_pack: ai
    link: https://scholar.google.com/citations?user=U36vPqgAAAAJ&hl
  - icon: orcid
    icon_pack: ai
    link: https://orcid.org/0009-0000-8959-966X
superuser: false
title: Gabriel Clara
---

I am a PhD student in mathematics at the Universiteit Twente. My current work focuses on the theoretical analysis of machine learning methods, in particular gradient descent training with added algorithmic noise. Typically, the problems involve non- asymptotic analysis of such algorithms and their statistical optimality. Previously, I worked on Bayesian variational inference.

Apart from my main research topics, I am interested in applications of differential and metric geometry, optimal transport, and functional analysis in probability and statistics. I keep up to date with relevant developments and try to find connections between these fields in my own work.

Feel free to [contact](#contact) me if you want to discuss anything, or simply talk about mathematics!
{style="text-align: justify;"}
